package com.anand.core;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.time.Duration;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;


import io.appium.java_client.MobileBy;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class Base {

    private DesiredCapabilities androidDesiredCapabilities() {
        File app = new File("src/test/resources/amazon-shopping-18-15-3-100.apk");
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
        desiredCapabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "AppiumPixelVD2");
        desiredCapabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
        desiredCapabilities.setCapability("noReset", "false");
        return desiredCapabilities;
    }

    public AndroidDriver<AndroidElement> launchAndroid() throws MalformedURLException {
        AndroidDriver<AndroidElement> driver;
        driver = new AndroidDriver<>(new URL("http://127.0.0.1:4723/wd/hub"), androidDesiredCapabilities());
        return driver;
    }


    public AndroidElement init(AndroidDriver driver, MobileBy by) {

        Wait wait = new FluentWait(driver)
                .withTimeout(Duration.ofSeconds(10))
                .pollingEvery(Duration.ofSeconds(5))
                .ignoring(NoSuchElementException.class);
        AndroidElement element;
        element = (AndroidElement) wait.until((Function<AndroidDriver, AndroidElement>) driver1 -> (AndroidElement) driver1.findElement(by));
        wait.until(ExpectedConditions.elementToBeClickable(by));
        return element;
    }
}

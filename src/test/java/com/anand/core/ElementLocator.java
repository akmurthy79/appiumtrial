package com.anand.core;

import io.appium.java_client.MobileBy;

public class ElementLocator {
    public static MobileBy.ByAndroidUIAutomator getByAmazonShoppingResourceId(String resourceSelectorId) {
                resourceSelectorId = "new UiSelector().resourceId(\"" +
                "com.amazon.mShop.android.shopping:id/" + resourceSelectorId + "\")";
        return new MobileBy.ByAndroidUIAutomator(resourceSelectorId);
    }

    public static MobileBy.ByAndroidUIAutomator getByResourceId(String resourceSelectorId) {
        resourceSelectorId = "new UiSelector().resourceId(\"" + resourceSelectorId + "\")";
        return new MobileBy.ByAndroidUIAutomator(resourceSelectorId);
    }

    //TODO
}

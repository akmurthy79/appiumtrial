package com.anand.steps;


import com.anand.core.Base;
import com.anand.core.ElementLocator;
import com.anand.pages.AmazonMShopPage;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import org.junit.Assert;

public class AmazonMShopPageSteps {
    Base base = new Base();
    AmazonMShopPage amazonMShopPage = new AmazonMShopPage();
    public AndroidDriver<AndroidElement> driver;

    @Given("^the user is on an (.*) device$")
    public void init(String deviceOS) throws Exception {
        if (deviceOS.equalsIgnoreCase("Android")) {
          driver = base.launchAndroid();
        }
    }
    @When("^the user logs in with userName (.*)$")
    public void launch(String userName) {

        base.init(driver, ElementLocator.getByAmazonShoppingResourceId(amazonMShopPage.SIGN_ON_BUTTON)).click();
        base.init(driver, ElementLocator.getByResourceId(amazonMShopPage.EMAIL_LOGIN_BUTTON)).sendKeys(userName);
        base.init(driver, ElementLocator.getByResourceId(amazonMShopPage.CONTINUE)).click();

    }

    @When("^the user logs in with password (.*)$")
    public void loginMShop(String password){
        base.init(driver, ElementLocator.getByResourceId(amazonMShopPage.PASSWORD_TEXTBOX)).sendKeys("Test4123@5");
        base.init(driver, ElementLocator.getByResourceId(amazonMShopPage.SIGNIN_SUBMIT_BUTTON)).click();
    }

    @Then("^(.*) is successfully logged in.$")
    public void the_user_is_successfully_logged_in(String name){
        base.init(driver, ElementLocator.getByAmazonShoppingResourceId(amazonMShopPage.ACTION_BAR_BURGER_ICON)).click();
        String welcomeMsg= base.init(driver, ElementLocator.getByAmazonShoppingResourceId(amazonMShopPage.GREETING_TEXTVIEW)).getText();
        Assert.assertTrue("User has Logged in successfully", welcomeMsg.contains(name));
    }

}
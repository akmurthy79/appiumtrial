package com.anand.pages;


public class AmazonMShopPage {

    public final String SIGN_ON_BUTTON = "sign_in_button";
    public final String EMAIL_LOGIN_BUTTON = "ap_email_login";
    public final String CONTINUE = "continue";
    public final String PASSWORD_TEXTBOX = "ap_password";
    public final String SIGNIN_SUBMIT_BUTTON = "signInSubmit";
    public final String ACTION_BAR_BURGER_ICON = "action_bar_burger_icon";
    public final String GREETING_TEXTVIEW = "gno_greeting_text_view";
}

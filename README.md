Getting Started
To run this project locally

Git clone https://gitlab.com/akmurthy79/appiumtrial.git

Prerequisites
1. Install ANDROID SDK 
2. Install JDK (Java Development Kit) 1.8
3. Install appium npm install -g appium
4. Make sure Android SDK and JAVA SDK are configured in ENV
5. Run appium-doctor to verify if everything is working as expected
5. Install Android SDK 
5. Use AVD manager to spin up a virtual Android Device 
6. Rename the device "AppiumPixelVD2"

# AppiumTrial
To run from Terminal:  mvn -Dtest=CukeRunner test

